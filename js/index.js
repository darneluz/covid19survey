$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000
    })

    $('#newsletter').on('show.bs.modal', function (e) {
        console.log('Modal newsletter se esta mostrando');
        $('#newsletterBtn').removeClass(' btn-outline-success');
        $('#newsletterBtn').addClass(' btn-primary');
        $('#newsletterBtn').prop('disabled', true);

    });
    $('#newsletter').on('shown.bs.modal', function (e) {
        console.log('modal newsletter se mostró');
    });

    $('#newsletter').on('hide.bs.modal', function (e) {
        console.log('modal newsletter se oculta');
    });
    $('#newsletter').on('hidden.bs.modal', function (e) {
        console.log('modal newsletter se ocultó');
        $('#newsletterBtn').prop('disabled', false);
        $('#newsletterBtn').removeClass(' btn-primary');
        $('#newsletterBtn').addClass('btn-outline-success');
    });

});